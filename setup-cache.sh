#!/bin/bash

set -eux

################################################################################
# Derived from 10_setup.sh
################################################################################

# Preprocess UPSTREAM_DNS to allow for multiple resolvers using the same syntax as lancache-dns
UPSTREAM_DNS="$(echo -n "${UPSTREAM_DNS}" | sed 's/[;]/ /g')"

echo "worker_processes ${NGINX_WORKER_PROCESSES};" > /etc/nginx/workers.conf

sed "s/^user .*/user ${WEBUSER};/g" /etc/nginx/nginx.conf.tpl > /etc/nginx/nginx.conf
sed "s/CACHE_INDEX_SIZE/${CACHE_INDEX_SIZE}/g"  /etc/nginx/conf.d/20_proxy_cache_path.conf.tpl | \
    sed "s/CACHE_DISK_SIZE/${CACHE_DISK_SIZE}/g" | \
    sed "s/CACHE_MAX_AGE/${CACHE_MAX_AGE}/g" > /etc/nginx/conf.d/20_proxy_cache_path.conf
sed "s/CACHE_MAX_AGE/${CACHE_MAX_AGE}/g"    /etc/nginx/sites-available/cache.conf.d/root/20_cache.conf.tpl | \
    sed "s/slice 1m;/slice ${CACHE_SLICE_SIZE};/g" > /etc/nginx/sites-available/cache.conf.d/root/20_cache.conf
sed "s/UPSTREAM_DNS/${UPSTREAM_DNS}/g"    /etc/nginx/sites-available/cache.conf.d/10_root.conf.tpl > /etc/nginx/sites-available/cache.conf.d/10_root.conf
sed "s/UPSTREAM_DNS/${UPSTREAM_DNS}/g"    /etc/nginx/sites-available/upstream.conf.d/10_resolver.conf.tpl > /etc/nginx/sites-available/upstream.conf.d/10_resolver.conf
sed "s/UPSTREAM_DNS/${UPSTREAM_DNS}/g"    /etc/nginx/stream-available/10_sni.conf.tpl > /etc/nginx/stream-available/10_sni.conf
sed "s/LOG_FORMAT/${NGINX_LOG_FORMAT}/g"  /etc/nginx/sites-available/10_cache.conf.tpl > /etc/nginx/sites-available/10_cache.conf
sed "s/LOG_FORMAT/${NGINX_LOG_FORMAT}/g"  /etc/nginx/sites-available/20_upstream.conf.tpl > /etc/nginx/sites-available/20_upstream.conf

################################################################################
# Derived from 15_generate_maps.sh
################################################################################

IFS=' '

TEMP_PATH=$(mktemp -d)
OUTPUTFILE=${TEMP_PATH}/outfile.conf
echo "map \"\$http_user_agent£££\$http_host\" \$cacheidentifier {" >> $OUTPUTFILE
echo "    default \$http_host;" >> $OUTPUTFILE
echo "    ~Valve\\/Steam\\ HTTP\\ Client\\ 1\.0£££.* steam;" >> $OUTPUTFILE
#Next line probably no longer needed as we are now regexing to victory
#echo "    hostnames;" >> $OUTPUTFILE
jq -r '.cache_domains | to_entries[] | .key' /data/cachedomains/cache_domains.json | while read CACHE_ENTRY; do 
	#for each cache entry, find the cache indentifier
	CACHE_IDENTIFIER=$(jq -r ".cache_domains[$CACHE_ENTRY].name" cache_domains.json)
	jq -r ".cache_domains[$CACHE_ENTRY].domain_files | to_entries[] | .key" cache_domains.json | while read CACHEHOSTS_FILEID; do
		#Get the key for each domain files
		jq -r ".cache_domains[$CACHE_ENTRY].domain_files[$CACHEHOSTS_FILEID]" cache_domains.json | while read CACHEHOSTS_FILENAME; do
            #Get the actual file name
            echo Reading cache ${CACHE_IDENTIFIER} from ${CACHEHOSTS_FILENAME}
			cat ${CACHEHOSTS_FILENAME} | while read CACHE_HOST; do
				#for each file in the hosts file
				#remove all whitespace (mangles comments but ensures valid config files)
				echo "host: $CACHE_HOST"
				CACHE_HOST=${CACHE_HOST// /}
				echo "new host: $CACHE_HOST"
				if [ ! "x${CACHE_HOST}" == "x" ]; then
					#Use sed to replace . with \. and * with .*
					REGEX_CACHE_HOST=$(sed -e "s#\.#\\\.#g" -e "s#\*#\.\*#g" <<< ${CACHE_HOST})
					echo "    ~.*£££.*?${REGEX_CACHE_HOST} ${CACHE_IDENTIFIER};" >> $OUTPUTFILE
				fi
			done
		done
	done
done
echo "}" >> $OUTPUTFILE
cat $OUTPUTFILE
cp $OUTPUTFILE /etc/nginx/conf.d/30_maps.conf
rm -rf $TEMP_PATH

unset IFS


################################################################################
# Derived from 20_perms_check.sh
################################################################################
# This breaks things, so don't do it.
#if [ -d "/data/cache/cache" ]; then
#	echo "Running fast permissions check"
#	ls -l /data/cache/cache | tail --lines=+2 | grep -v ${WEBUSER} > /dev/null
#
#	if [[ $? -eq 0 || "$FORCE_PERMS_CHECK" == "true" ]]; then
#		echo "Doing full checking of permissions (This WILL take a long time on large caches)..."
#		find /data \! -user ${WEBUSER} -exec chown ${WEBUSER}:${WEBUSER} '{}' +
#		echo "Permissions ok"
#	else
#		echo "Fast permissions check successful, if you have any permissions error try running with -e FORCE_PERMS_CHECK = true"
#	fi
#fi

################################################################################
# Derived from 99_config_check.sh
################################################################################
echo "Currently configured config:"
/scripts/getconfig.sh /etc/nginx/nginx.conf

echo "Checking nginx config"
/usr/sbin/nginx -t

 [ $? -ne 0 ] || echo "Config check successful"
