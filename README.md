
# Lancache-LXC
Lancache, but for LXC (Linux Containers) instead of Docker.

# About
Files from `cache-domains` and `base` are taken from the
[uk-lans](https://github.com/uklans/cache-domains) project and
[lancache](https://lancache.net/) projects respectively.

It was a pain in the butt to build these, but it works okay.
**You may need to do minor tweaking to get these to work on your setup.**
Follow the build instructions for how to setup these container images
correctly.

# Build
First you'll need to install the LXC build tools like `distrobuilder`.

After you've checked out this repo, be sure to checkout the proper `git`
sub-modules as well:
```
$ git clone https://gitlab.com/camconn/lancache-lxc
$ cd lancache-lxc
$ git submodule init
$ git submodule update
```

## Base Ubuntu Image
First build the (outdated) base Ubuntu image.
```
$ sudo distrobuilder build-lxc ubuntu.yml
```

## Base Monolithic Image
This builds the image that will actually hold the files.
Point the DNS container at this container's IP.
```
$ sudo distrobuilder build-lxc monolithic.yml
```

Your monolithic file-hosting container image will now be in the
`monolithic` directory.

## Base DNS Image
This builds the DNS server that points to the container with the
cached files (the monolithic image).

Modify the value of `LANCACHE_IP` to where your monolithic container will
run in both `dns.yml` and `dns-config.yml`. Then run the build command et voila.

```
$ sudo distrobuilder build-lxc dns.yml
```

Your DNS container image will now be in the `dns` directory.

# License
All files in this repo are licensed under the MIT license. See `LICENSE` for
more details.

- Files from the cache-domains project are licensed under the MIT license.
- Files from the lancache project are licensed under the MIT license.
- Files derived from these projects are released under the MIT license.
- All other files in this project are licensed under the MIT license.
